import setuptools

setuptools.setup(
    name="pwndbg", # Replace with your own username
    version="2019.12.09",
    author="Jonathan",
    author_email="jonathan@pocydon.com",
    description="GDB plug-in",
    long_description="GDB plug-in that makes debugging with GDB suck less, with a focus on features needed by low-level software developers, hardware hackers, reverse-engineers and exploit developers.",
    long_description_content_type="text/markdown",
    url="https://gitlab.com/twosix-kali/packages1/pwndbg",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)

